FactoryBot.define do
  factory :day do
    date { Time.parse('2018-06-06') }
  end
end
