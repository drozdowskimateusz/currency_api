FactoryBot.define do
  factory :currency do
    code { 'USD' }
    rate { 1.23 }
  end
end
