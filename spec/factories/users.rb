FactoryBot.define do
  factory :user do
    sequence(:email) { |n| "joe.doe#{n}@example.com" }
    password { 'password' }
  end
end
