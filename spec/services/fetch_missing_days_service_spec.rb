require 'rails_helper'

describe FetchMissingDaysService do
  let(:start_date) { Time.current.beginning_of_day }
  let(:end_date) { (Time.current+2.days).end_of_day }
  subject(:fetch_days_service) { described_class.new(start_date, end_date) }

  describe '#call' do
    context 'with 2 dates with gap of 1 day between them' do
      it 'should return array of 3 dates' do
        allow_any_instance_of(GetCurrencyService).to receive(:call).and_return(1)
        expect(subject.call.length).to eq 3
      end
    end

  end
end
