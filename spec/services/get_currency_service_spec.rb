require 'rails_helper'

describe GetCurrencyService do
  subject(:valid_data_object) { described_class.new(Time.current) }
  subject(:invalid_data_object) { described_class.new('123') }
  let(:return_json) { { 'rates': ['mid': 2.23] }.to_json }

  describe '#call' do
    context 'with valid date' do
      it 'should return true' do
        expect(OpenURI).to receive(:open_uri).twice.and_return(return_json)
        expect(valid_data_object.call).to be true
      end
    end

    context 'when invalid date' do
      it 'should raise error' do
        expect { invalid_data_object.call }.to raise_error(NoMethodError)
      end
    end
  end
end
