module RequestSpecHelper
  def valid_headers
    {
      'Authorization' => JsonWebTokenService.encode(user_id: user.id),
      'Content-Type' => 'application/json'
    }
  end
end
