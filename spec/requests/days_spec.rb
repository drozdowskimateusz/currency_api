require 'rails_helper'

describe 'DaysController', type: :request do
  let!(:user) { create :user }
  let(:headers) { valid_headers }
  let!(:day) { create :day }
  let!(:usd_currency) { create :currency, day_id: day.id }

  describe 'GET #index' do
    context 'with one of dates missing' do
      before { get '/days?start_date=2018-06-06', headers: valid_headers }
      it 'should return missing date error message and 400 status code' do
        expect(response.body).to match(/Missing start or end date/)
        expect(response).to have_http_status(400)
      end
    end

    context 'with invalid format' do
      before { get '/days?start_date=asdf', headers: valid_headers }
      it 'should return invalid format error message and 400 status code' do
        expect(response.body).to match(/Invalid date format/)
        expect(response).to have_http_status(400)
      end
    end
  end

  describe 'GET #index' do
    context 'with valid data' do
      before { get '/days/2018-06-06', headers: valid_headers }
      it 'should return valid json response' do
        expect(JSON.parse(response.body)['currencies'].first['rate']).to eq '1.23'
        expect(response).to have_http_status(200)
      end
    end

    context 'with date not in database' do
      before { get '/days/2018-06-26', headers: valid_headers }
      it 'should return valid empty json response' do
        expect(response.body).to match(/Couldn't find Day/)
        expect(response).to have_http_status(404)
      end
    end

    context 'with invalid format' do
      before { get '/days/asdf', headers: valid_headers }
      it 'should return invalid format error message and 400 status code' do
        expect(response.body).to match(/Invalid date format/)
        expect(response).to have_http_status(400)
      end
    end
  end
end
