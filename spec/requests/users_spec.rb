require 'rails_helper'

describe 'Users API', type: :request do
  let(:user) { build(:user) }
  let(:headers) { { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' } }
  let(:valid_attributes) do
    attributes_for(:user, password_confirmation: user.password)
  end

  describe 'POST /users' do
    context 'when valid request' do
      before { post '/users', params: valid_attributes.to_json, headers: headers }

      it 'creates a new user' do
        expect(response).to have_http_status(201)
      end

      it 'returns success message' do
        expect(JSON.parse(response.body)['message']).to match(/Account created/)
      end

      it 'returns an authentication token' do
        expect(JSON.parse(response.body)['auth_token']).not_to be_nil
      end
    end

    context 'when invalid request' do
      before { post '/users', params: {}, headers: headers }

      it 'does not create a new user' do
        expect(response).to have_http_status(422)
      end

      it 'returns failure message' do
        expect(JSON.parse(response.body)['message']).to match(
          /Validation failed: Password can't be blank, Email can't be blank, Password digest can't be blank/
        )
      end
    end
  end
end
