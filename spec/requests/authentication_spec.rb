require 'rails_helper'

describe 'Authentication', type: :request do
  describe 'POST /authentications/create' do
    let!(:user) { create(:user) }
    let(:headers) { { 'Content-Type' => 'application/json' } }
    let(:valid_credentials) do
      {
        email: user.email,
        password: user.password
      }.to_json
    end
    let(:invalid_credentials) do
      {
        email: 'invalid_email',
        password: 'invalid_password'
      }.to_json
    end

    context 'When request is valid' do
      before { post '/authentications', params: valid_credentials, headers: headers }

      it 'returns an authentication token' do
        expect(JSON.parse(response.body)['auth_token']).not_to be_nil
      end
    end

    context 'When request is invalid' do
      before { post '/authentications', params: invalid_credentials, headers: headers }

      it 'returns a failure message' do
        expect(JSON.parse(response.body)['message']).to match(/Invalid credentials/)
      end
    end
  end
end
