class CreateCurrencies < ActiveRecord::Migration[5.2]
  def change
    create_table :currencies do |t|
      t.string :code
      t.decimal :rate
      t.integer :day_id

      t.timestamps
    end
  end
end
