class DaySerializer < ActiveModel::Serializer
  attributes :date

  has_many :currencies

  class CurrencySerializer < ActiveModel::Serializer
    attributes :rate, :code
  end
end
