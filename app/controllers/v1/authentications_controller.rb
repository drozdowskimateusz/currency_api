module V1
  class AuthenticationsController < ApplicationController
    skip_before_action :authorize_request, only: :create

    def create
      render json: { auth_token: auth_token }
    end

    private

    def auth_token
      @auth_token ||= AuthenticateUserService.new(auth_params[:email], auth_params[:password]).call
    end

    def auth_params
      params.permit(:email, :password)
    end
  end
end
