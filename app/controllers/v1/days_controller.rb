module V1
  class DaysController < ApplicationController
    def index
      FetchMissingDaysService.new(start_date, end_date).call
      render json: days(start_date, end_date), status: :ok
    end

    def show
      render json: day, status: :ok
    end

    private

    def start_date
      return if params[:start_date].blank?
      @start_date ||= Time.parse(params[:start_date]).beginning_of_day
    end

    def end_date
      return if params[:end_date].blank?
      @end_date ||= Time.parse(params[:end_date]).end_of_day
    end

    def day
      @day ||= Day.find_by!(date: Time.parse(params[:date]).beginning_of_day..Time.parse(params[:date]).end_of_day)
    end

    def days(start_date, end_date)
      @days ||= Day.where(date: start_date..end_date)
    end
  end
end
