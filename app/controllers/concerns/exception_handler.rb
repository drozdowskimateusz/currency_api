module ExceptionHandler
  extend ActiveSupport::Concern

  AuthenticationError = Class.new(StandardError)
  MissingToken = Class.new(StandardError)
  InvalidToken = Class.new(StandardError)

  included do
    rescue_from ActiveRecord::RecordInvalid, with: :four_twenty_two
    rescue_from ArgumentError, with: :fourhundred
    rescue_from FetchMissingDaysService::MissingDate do
      render json: { message: 'Missing start or end date' }, status: 400
    end
    rescue_from ActiveRecord::RecordNotFound do |e|
      render json: { message: e.message }, status: :not_found
    end
    rescue_from ExceptionHandler::AuthenticationError, with: :unauthorized_request
    rescue_from ExceptionHandler::MissingToken, with: :four_twenty_two
    rescue_from ExceptionHandler::InvalidToken, with: :four_twenty_two
  end

  private

  def fourhundred
    render json: { message: 'Invalid date format' }, status: 400
  end

  def four_twenty_two(e)
    render json: { message: e.message }, status: :unprocessable_entity
  end

  def unauthorized_request(e)
    render json: { message: e.message }, status: :unauthorized
  end
end
