class Currency < ApplicationRecord
  belongs_to :day

  validates :code, :rate, :day_id, presence: true
end
