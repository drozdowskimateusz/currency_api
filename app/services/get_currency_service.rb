require 'open-uri'

class GetCurrencyService
  def initialize(date, unparsed = nil)
    @date = date
    @unparsed = unparsed
  end

  def call
    ActiveRecord::Base.transaction do
      if unparsed
        request_date = date
      else
        request_date = date.strftime('%Y-%m-%d')
      end
      usd = JSON.load(open("http://api.nbp.pl/api/exchangerates/rates/a/usd/#{request_date}/?format=json"))
      eur = JSON.load(open("http://api.nbp.pl/api/exchangerates/rates/a/eur/#{request_date}/?format=json"))
      day = Day.create!(date: date)
      day.currencies.create!(code: 'USD', rate: usd['rates'].first['mid'])
      day.currencies.create!(code: 'EUR', rate: eur['rates'].first['mid'])
      return true
    end
  end

  private

  attr_reader :date, :unparsed
end
