class FetchMissingDaysService
  MissingDate = Class.new(StandardError)

  def initialize(start_date, end_date)
    @start_date = start_date
    @end_date = end_date
  end

  def call
    raise MissingDate if start_date.blank? || end_date.blank?

    missing_dates = all_days_in_range - found_days
    missing_dates.each do |date|
      GetCurrencyService.new(date, true).call
    end
    missing_dates
  end

  private

  def found_days
    @found_days ||= Day.where(date: start_date..end_date).sort.map { |x| x.date.strftime('%Y-%m-%d') }
  end

  def all_days_in_range
    @all_days_in_range ||= (start_date.to_date..end_date.to_date).sort.map{ |date| date.strftime('%Y-%m-%d') }
  end

  attr_reader :start_date, :end_date
end
