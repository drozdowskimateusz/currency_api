Rails.application.routes.draw do
  scope module: :v1, constraints: ApiVersion.new('v1', true) do
    resources :authentications, only: :create
    resources :days, only: [:show, :index], param: :date
    resources :users, only: :create
  end
end
