namespace :get_currency do
  desc "Get today's currencies"
  task today: :environment do
    GetCurrencyService.new(Time.current).call
  end
end
